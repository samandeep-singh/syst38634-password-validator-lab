package password;

import static org.junit.Assert.*;

import org.junit.Test;

/**
 * 
 * @author Samandeep Singh 991500155
 * 
 * Assuming that a password cannot contain any spaces
 *
 */

public class PasswordValidatorTest {
	
	
	@Test
	public void testHasValidCaseCharsRegular() {
		assertTrue("Invalid case chars", PasswordValidator.hasValidCaseChars("grddWrrr"));
	}
	
	@Test
	public void testHasValidCaseCharsBoundaryIn() {
		assertTrue("Invalid case chars", PasswordValidator.hasValidCaseChars("Aa"));
	}
	
	@Test
	public void testHasValidCaseCharsException() {
		assertFalse("Invalid case chars", PasswordValidator.hasValidCaseChars("12345"));
	}
	
	@Test
	public void testHasValidCaseCharsExceptionEmpty() {
		assertFalse("Invalid case chars", PasswordValidator.hasValidCaseChars(""));
	}
	
	@Test
	public void testHasValidCaseCharsExceptionNull() {
		assertFalse("Invalid case chars", PasswordValidator.hasValidCaseChars(null));
	}
	
	@Test
	public void testHasValidCaseCharsBoundaryOutUpper() {
		assertFalse("Invalid case chars", PasswordValidator.hasValidCaseChars("A"));
	}
	
	@Test
	public void testHasValidCaseCharsBoundaryOutLower() {
		assertFalse("Invalid case chars", PasswordValidator.hasValidCaseChars("a"));
	}
	
	
	@Test
	public void testIsValidLengthBoundaryIn() {
		boolean isValid = PasswordValidator.isValidLength("b#y5V7(8");
		assertTrue("Invalid Password length!", isValid);

	}
	
	@Test
	public void testIsValidLengthBoundaryOut() {
		boolean isValid = PasswordValidator.isValidLength("m!y9PbQ");
		assertFalse("Invalid Password length!", isValid);
	}
	
	@Test
	public void testIsValidLengthExceptionSpace() {
		boolean isValid = PasswordValidator.isValidLength("JUnit is amazing!");
		assertFalse("Invalid Password Format!", isValid);
	}
	
	@Test
	public void testIsValidLengthExceptionNull() {
		boolean isValid = PasswordValidator.isValidLength(null);
		assertFalse("Invalid Password Format!", isValid);
	}
	
	@Test
	public void testHasValidDigitCountRegular() {
		boolean hasValidDigits = PasswordValidator.hasValidDigitCount("n7Ml2.8k0v3");
		assertTrue("Invalid Digit Count!", hasValidDigits);
	}
	
	@Test
	public void testHasValidDigitCountException() {
		boolean hasValidDigits = PasswordValidator.hasValidDigitCount("lkm&bWqodp");
		assertFalse("Invalid Digit Count!", hasValidDigits);
	}
	
	@Test
	public void testHasValidDigitCountBoundaryIn() {
		boolean hasValidDigits = PasswordValidator.hasValidDigitCount("m4e$pLnV2kq");
		assertTrue("Invalid Digit Count!", hasValidDigits);
	}
	
	@Test
	public void testHasValidDigitCountBoundaryOut() {
		boolean hasValidDigits = PasswordValidator.hasValidDigitCount("nq9#vkUq!ddj");
		assertFalse("Invalid Digit Count!", hasValidDigits);
	}

}
